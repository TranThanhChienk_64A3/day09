<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../day09/style.css">
    <style>
        body {
            display: flex;
            justify-content: center;
            background-color: #A5BECC;
            color: #FFFFFF;
        }
    </style>
</head>

<body>
    <?php
    session_start();
    $check = array();
    $answer = $_SESSION["answer"];

    for ($i = 1; $i <= 10; $i++) {
        $key = "question_" . strval($i);
        $check[] = $_SESSION[$key];
    }
    $ans_success = array();
    foreach ($answer as $key_ans => $value_ans) {
        foreach ($value_ans as $vlue => $dapan) {
            if ($dapan == 1) {
                $ans_success[] = $vlue;
            }
        }
    }
    $match = 0;
    for ($i = 0; $i < 10; $i++) {
        if ($check[$i] == $ans_success[$i]) {
            $match++;
        }
    }
    ?>
    <form action="" method="GET">
        <p>Số điểm của bạn là: <?php echo $match ?></p>
        <p> Đánh giá:
            <?php
            if ($match < 4) {
                echo "Bạn quá kém, cần ôn tập thêm.";
            } elseif ($match <= 7) {
                echo "Cũng bình thường";
            } else {
                echo "Sắp sửa làm được trợ giảng lớp PHP.";
            }
            ?>
        </p>
    </form>
</body>

</html>
