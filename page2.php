<?php

session_start();
$ans_page_1 = $_SESSION;
$question = array(
    1 => "PHP tượng trưng cho cái gì:",
    2 => "Mặc định của một biến không có giá trị được thể hiện với từ khóa",
    3 => "Hàm nào sau đây dùng để khai báo hằng số",
    4 => "Con chó tiếng anh là gì?",
    5 => "Con mèo nhà Chiến tên gì?",
    6 => "1+1= ",
    7 => "quả chuối chín thường có màu gì?",
    8 => "mèo có đáng yêu không?",
    9 => "một ngày có bao nhiêu giờ?",
    10 => "bài day09 của chiến được mấy điểm"
);

$answer = array(
    1 => array("Preprocessed Hypertext Page" => 0, "Hypertext Transfer Protocol" => 0, "Hypertext Markup Language" => 0, "PHP: Hypertext Preprocessor" => 1),
    2 => array("none" => 0, "null" => 1, "không có giá trị nào như vậy trong PHP" => 0, "undef" => 0),
    3 => array("const" => 0, "constants" => 0, "define" => 1, "def" => 0),
    4 => array("dog" => 1, "cat" => 0, "bull" => 0, "pig" => 0),
    5 => array("gâu" => 0, "bruh" => 0, "jo" => 0, "Pè" => 1),
    6 => array("2" => 1, "1" => 0, "3" => 0, "0" => 0),
    7 => array("đỏ" => 0, "vàng" => 1, "tím" => 0, "đen" => 0),
    8 => array("có" => 0, "có có có" => 0, "không" => 0, "có*3.14" => 1),
    9 => array("12" => 0, "24" => 1, "36" => 0, "23" => 0),
    10 => array("9" => 0, "9.5" => 0, "10" => 1, "8" => 0)
);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $_SESSION = $ans_page_1;
    for ($i = 6; $i <= 10; $i++) {
        $key = "question_" . strval($i);
        $_SESSION[$key] = $_POST["$key"];
    }
    $_SESSION["answer"] = $answer;
    header("Location: submit.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<!--head-->

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../day09/style.css">
    <style>
        body {
            display: flex;
            justify-content: center;
            background-color: #A5BECC;
            color: #FFFFFF;
        }

        .container {
            display: inline-block;
            width: 600px;
        }

        .title-field {
            font-size: 30px;
            color: #243A73;
            margin: 10px;
            margin-left: 0px;
        }

        .question-box {
            padding: 10px 0px;
            padding-left: 10px;
            border: 1px solid rgba(206, 206, 206, 255);
            border-radius: 7px;
            margin-top: 7px;
            background-color: white;
        }

        .answer-field {
            margin-top: 5px;
        }

        .button-submit {
            font: 16px "Courier";
            font-weight: bold;
            margin-top: 10px;
            float: middle;
            padding: 10px 30px;
            background-color: #F2EBE9;
            border: none;
            color: #354259;
            border-radius: 5px;

        }
    </style>
</head>

<!--body-->

<body>

    <div class="container" style>
        <h1 class="title-field">Fun Quiz</h1>
        <form method="POST" enctype="multipart/form-data" action="<?php
            echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            <?php
            foreach ($question as $key => $value) {
                if ($key >= 6) {
                    echo "<p>Câu {$key}: {$value}</p>";
                    foreach ($answer as $key_ans => $value_ans) {
                        if ($key == $key_ans) {
                            foreach ($value_ans as $vlue => $dapan) {
                                echo "<input type=\"radio\" name=\"question_{$key_ans}\" value=\"{$vlue}\"> {$vlue}<br>";
                            }
                        }
                    }
                }
            };
            ?>
            <button name="submit" id="button-submit" class="button-submit" type="submit">Submit</button>
        </form>
    </div>
</body>


</html>